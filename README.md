<p align="center">
  <img src="./animation.gif" />
</p>

# 3D Card Effect

The 3D effect here is done by playing with the perspective of the card based on the position of mouse relative to the card. The effect is acheved by the following methods.

### Rotation

The card is rotated based on the how far the mouse is in relation to the center of the card.

### Image Translation

The image is translated in the X and Y axis based on the mouse position to give an effect of Parallax.

### Text Translation

The title and the content text are translated in the Z axis when hovered on to give it the final 3D touch.
