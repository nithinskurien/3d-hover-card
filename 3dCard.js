//DOM elements
const card = document.querySelector(".card-container");
const container = document.querySelector(".card-hover-container");
const background = document.querySelector(".card-background");
const title = document.querySelector(".card-title");
const content = document.querySelector(".card-content");

//Constants
const SCALING_FACTOR = 1.05;
const ROTATION_FACTOR = 10;
const TRANSLATION_FACTOR = 40;
const Z_TITLE_TRANSLATION = "2rem";
const Z_CONTENT_TRANSLATION = "1rem";
const TRANSITION_DURATION = 250;
const TRANSITION_CUBIC_ENTER = "cubic-bezier(0.63, 1, 0.28, 1)";
const TRANSITION_CUBIC_EXIT = "cubic-bezier(0.445, 0.05, 0.55, 0.95)";

/* 
  Get the mouse offset from the centre of the hover container
  returns an array of mouse offset [x,y]
*/
const getMouseOffset = (e) => {
  // Get the center of the container
  let xPosContainer = container.offsetLeft + container.offsetWidth / 2;
  let yPosContainer = container.offsetTop + container.offsetHeight / 2;
  // Get the mouse offset from the centre
  let xAxis = (e.pageX - xPosContainer) / container.offsetWidth;
  let yAxis = -(e.pageY - yPosContainer) / container.offsetHeight;
  return [xAxis, yAxis];
};

/* 
  Mousemove listener on the hover container to rotate and translate the container and background 
*/
container.addEventListener("mousemove", (e) => {
  const [xAxis, yAxis] = getMouseOffset(e);
  card.style.transform = `rotateY(${xAxis * ROTATION_FACTOR}deg) rotateX(${
    yAxis * ROTATION_FACTOR
  }deg) scale(${SCALING_FACTOR})`;
  background.style.transform = `translateX(${
    -xAxis * TRANSLATION_FACTOR
  }px) translateY(${yAxis * TRANSLATION_FACTOR}px)`;
});

/* 
  Mouseenter listener on the hover container to add transitions to smoothen the animation from non hover to hover
*/
container.addEventListener("mouseenter", (e) => {
  // Card animation needs to transition smoothly when mouse hovers over it instead of jumping to the new state
  card.style.transition = `all ${
    TRANSITION_DURATION / 1000
  }s ${TRANSITION_CUBIC_ENTER}`;
  background.style.transition = `all ${
    TRANSITION_DURATION / 1000
  }s ${TRANSITION_CUBIC_ENTER}`;
  // Card info elements are translated in z direction when mouse is hovered over card container
  title.style.transform = `translateZ(${Z_TITLE_TRANSLATION})`;
  content.style.transform = `translateZ(${Z_CONTENT_TRANSLATION})`;
  // Adding timeout before the transition effect for card & background is removed,
  // else the "mousemove" animation will start transitioning between each movement, feeling unresponsive/sluggish.
  // The timeout should be equal to the transition duration above
  setTimeout(() => {
    card.style.transition = "none";
    background.style.transition = "none";
  }, TRANSITION_DURATION);
});

/* 
  Mouseleave listener on the hover container to add transitions to smoothen the animation from hover to non-hover
  and reset the Card to the initial position
*/
container.addEventListener("mouseleave", (e) => {
  // Adding transition so that we can animate smoothly to the inital state from the "mousemove" state
  card.style.transition = `all ${
    TRANSITION_DURATION / 1000
  }s ${TRANSITION_CUBIC_EXIT}`;
  background.style.transition = `all ${
    TRANSITION_DURATION / 1000
  }s ${TRANSITION_CUBIC_EXIT}`;
  // Reset the styles of all the elements to their initial state
  card.style.transform = `rotateY(0deg) rotateX(0deg) scale(1)`;
  background.style.transform = "translateX(0) translateY(0)";
  title.style.transform = "translateZ(0)";
  content.style.transform = "translateZ(0)";
});
